/* eslint-env node */

module.exports = {
    siteMetadata: {
        title: process.env.TITLE,
    },
    plugins: [
        // Helmet configuration
        "gatsby-plugin-react-helmet",

        // Configure source filesystem images path
        {
            resolve: "gatsby-source-filesystem",
            options: {
                name: "images",
                path: `${__dirname}/src/images`,
            },
        },
        "gatsby-plugin-sharp",
        "gatsby-transformer-sharp",
        "gatsby-transformer-sqip",

        // Favicons and web manifest
        {
            resolve: "gatsby-plugin-manifest",
            options: {
                name: process.env.TITLE,
                short_name: process.env.TITLE,
                start_url: "/",
                background_color: "#3b3b39",
                theme_color: "#3b3b39",
                display: "standalone",
                icon: "src/images/favicon.png", // This path is relative to the root of the site.
            },
        },

        // Offline functionality
        // "gatsby-plugin-offline",

        // Sass support
        {
            resolve: "gatsby-plugin-sass",
            options: {
                includePaths: [`${__dirname}/node_modules`],
            },
        },

        // Eslinting
        "gatsby-plugin-eslint",

        // Style linting
        {
            resolve: "@danbruegge/gatsby-plugin-stylelint",
            options: { files: ["src/**/*.{js,css,scss}"] },
        },
    ],
};
