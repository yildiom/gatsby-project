// @flow

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Router } from "@reach/router";

const WrapperRoute = ({ content }: *) => content;

export default class Wrapper extends PureComponent<*, *> {
    static propTypes = {
        children: PropTypes.node.isRequired,
        onError: PropTypes.func.isRequired,
    };

    componentDidCatch(error: *) {
        this.props.onError(error);
    }

    render() {
        return (
            <Router>
                <WrapperRoute content={this.props.children} default />
            </Router>
        );
    }
}
