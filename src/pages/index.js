// @flow

import React from "react";
import { Link } from "gatsby";
import Layout from "../components/layout";

const IndexPage = () => (
    <Layout>
        <h1>Hello</h1>
        <h2>I am Omer. A front-end developer living in Belgium</h2>
        <p>
            Need a developer?
            <Link to="/contact">Contact me</Link>
        </p>
    </Layout>
);

export default IndexPage;
