// @flow

import React from "react";
import { Link } from "gatsby";
import Layout from "../components/layout";

const About = () => (
    <Layout>
        <h1>About</h1>
        <p>This is a website about me and my works</p>
        <p>
            Do you want to <Link to="/contact">contact me?</Link>
        </p>
    </Layout>
);

export default About;
