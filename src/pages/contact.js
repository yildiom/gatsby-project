// @flow

import React from "react";
import Layout from "../components/layout";

const Contact = () => (
    <Layout>
        <h1>Contact</h1>
        <p>
            E-mail:{" "}
            <a href="mailto:omeryildiz54@gmail.com">omeryildiz54@gmail.com</a>
        </p>
        <p>
            Github:{" "}
            <a
                href="https://github.com/yildiom"
                rel="noopener noreferrer"
                target="_blank"
            >
                Github Page
            </a>
        </p>
    </Layout>
);

export default Contact;
