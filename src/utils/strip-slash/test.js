// @flow

import { strip, stripTrailing, stripLeading } from "./";

const MOCK_NONE = "foo/bar/baz";
const MOCK_LEADING = "/foo/bar/baz";
const MOCK_TRAILING = "foo/bar/baz/";
const MOCK_BOTH = "/foo/bar/baz/";

test("Strips leading and trailing slashes", () =>
    expect(strip(MOCK_BOTH)).toBe(MOCK_NONE));

test("Strips leading slashes", () =>
    expect(stripLeading(MOCK_BOTH)).toBe(MOCK_TRAILING));

test("Strips nothing if there are none", () =>
    expect(strip(MOCK_NONE)).toBe(MOCK_NONE));

test("Strips trailing slashes", () =>
    expect(stripTrailing(MOCK_BOTH)).toBe(MOCK_LEADING));
