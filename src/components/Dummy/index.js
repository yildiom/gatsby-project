// @flow

import style from "./style.module.scss";

import React from "react";

/**
 * Dummy
 */
const Dummy = ({ children }: *) => <p className={style.element}>{children}</p>;

export default Dummy;
