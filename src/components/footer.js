// @flow

import React from "react";

const Footer = () => (
    <footer>
        <p>Created by Omer Yildiz, © 2019</p>
    </footer>
);

export default Footer;
