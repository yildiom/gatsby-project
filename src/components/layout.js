// @flow

import * as React from "react";
import Header from "./header";
import Footer from "./footer";

type Props = {
    children?: React.Node,
};

const Layout = (props: Props) => (
    <div>
        <Header />
        {props.children}
        <Footer />
    </div>
);

export default Layout;
