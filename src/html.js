// @flow

import React, { Component, type Node } from "react";

type Props = {
    htmlAttributes: Object,
    headComponents: Node,
    bodyAttributes: Object,
    preBodyComponents: Node,
    body: string,
    postBodyComponents: Node,
};

export default class HTML extends Component<Props> {
    render() {
        return (
            <html lang="en" {...this.props.htmlAttributes}>
                <head>
                    <meta charSet="utf-8" />
                    <meta httpEquiv="x-ua-compatible" content="ie=edge" />
                    <meta
                        name="viewport"
                        content="width=device-width, initial-scale=1, shrink-to-fit=no"
                    />
                    {this.props.headComponents}
                </head>
                <body {...this.props.bodyAttributes}>
                    {this.props.preBodyComponents}
                    <div
                        key={`body`}
                        id="___gatsby"
                        dangerouslySetInnerHTML={{ __html: this.props.body }}
                    />
                    {this.props.postBodyComponents}
                </body>
            </html>
        );
    }
}
